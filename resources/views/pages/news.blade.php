@extends('layout')
@section('news')

    <div class="grid-container news-list" style="max-width: 1600px; margin-top: 8em;">
        <div class="grid-x">
            <div class="cell small-12 medium-6">
                <h3>LATEST NEWS</h3>
                <hr>
                <div class="grid-x news-block">
                    <div class="cell small-12 medium-4">
                        <img src="{{ asset('img/flash-war.jpg') }}" class="news-img" style="padding: 1em;">
                    </div>
                    <div class="cell small-12 medium-8" style="padding: 1em;">
                        <h3 class="news-title">The Flash War is here!</h3>
                        <div class="published"><p>By Peter Boersma <strong>21/06/2018</strong></p></div>
                        <div class="news-description"><p>Barry Allen vs Wally West, Is this the biggest fight in DC History?!</p></div>
                    </div>
                </div>
                <hr class="news-divid">
                <a href="/">
                <div class="grid-x news-block">
                    <div class="cell small-12 medium-4">
                        <img src="{{ asset('img/DC-Featured3.jpg') }}" class="news-img" style="padding: 1em;">
                    </div>
                    <div class="cell small-12 medium-8" style="padding: 1em;">
                        <h3 class="news-title">Batman: Prelude to the Wedding</h3>
                        <div class="published"><p>By Peter Boersma <strong>21/06/2018</strong></p></div>
                        <div class="news-description"><p>Joker crashes Bruce's and Selina's Wedding. What is Batman going to do?</p></div>
                    </div>
                </div>
                </a>
                <hr class="news-divid">
                <a href="/">
                    <div class="grid-x news-block">
                        <div class="cell small-12 medium-4">
                            <img src="{{ asset('img/jlrebirth.jpg') }}" class="news-img" style="padding: 1em;">
                        </div>
                        <div class="cell small-12 medium-8" style="padding: 1em;">
                            <h3 class="news-title">The Justice League is back!</h3>
                            <div class="published"><p>By Peter Boersma <strong>04/07/2018</strong></p></div>
                            <div class="news-description"><p>The Justice League is recovering from the events of No Justice</p></div>
                        </div>
                    </div>
                </a>
                <hr class="news-divid">
                <a href="/">
                    <div class="grid-x news-block">
                        <div class="cell small-12 medium-4">
                            <img src="{{ asset('img/dknight.jpg') }}" class="news-img" style="padding: 1em;">
                        </div>
                        <div class="cell small-12 medium-8" style="padding: 1em;">
                            <h3 class="news-title">Green Lantern: Darkest Night</h3>
                            <div class="published"><p>By Peter Boersma <strong>21/06/2018</strong></p></div>
                            <div class="news-description"><p>Death is coming for the Justice League</p></div>
                        </div>
                    </div>
                </a>
                <hr class="news-divid">
            </div>
            <div class="cell small-1"></div>
            <div class="cell small-12 medium-5">
                <div class="old-news">
                    <div class="old-news-heading">
                        <h3>THIS MONTH</h3>
                    </div>
                    <div class="news-post-list">
                        <div class="news-post">
                            <a href="#"><span class="news-title">The 3 Jokers Theory</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">New Lantern light discovered</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">Welcome Terry to the Bat Family</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">Who is Mr. Oz?</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">Hal Jordan vs John Stewart</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">Who killed Eobard Thawne?</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">The Sage force and the Strenght Force?</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">The new DC Universe Streaming service</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">Young Justice: Outsiders season 3 delayed</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                        <div class="news-post">
                            <a href="#"><span class="news-title">Ben Affleck not playing Batman in the next movie</span><span class="news-date">19 February 2018</span></a>
                            <hr>
                        </div>
                    </div>

                </div>
                <img style="padding: 3em; height: 20em; float:right" src="{{asset('img/logo-background-v2.png')}}">
            </div>
        </div>
    </div>

@endsection
