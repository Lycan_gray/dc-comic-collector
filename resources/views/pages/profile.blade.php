@extends('layout')
@section('profile')

<div class="grid-container profile">
    <div class="profile-block">
        <div class="grid-x">
            <div class="cell small-1"><img class="profile-img" src="{{asset('img/pf.png')}}"></div>
            <div class="cell small-8">
                <div class="profile-basic-info">
                    <h2>Lycan</h2>
                    <p>Joined on 01/07/2018</p>
                </div>
            </div>
        </div>
    </div>

    <div class="profile-info">
        <div class="grid-x">
            <div class="cell small-6 ">
                <h4>Personal Info</h4>
                <hr>
                <table class="detail-info" style="width:100%">
                    <tr>
                        <td id="table-imp">Name</td>
                        <td>Peter Boersma</td>
                    </tr>
                    <tr>
                        <td id="table-imp">Birthday</td>
                        <td>09/08/1999</td>
                    </tr>
                    <tr>
                        <td id="table-imp">Gender</td>
                        <td>Male</td>
                    </tr>
                    <tr>
                        <td id="table-imp">Country</td>
                        <td>The Netherlands</td>
                    </tr>
                </table>
            </div>
            <div class="cell small-1"></div>
            <div class="cell small-5 ">
                <h4>Subscribed Titles</h4>
                <hr>
                <table style="width:100%">
                    <tr>
                        <td id="table-imp">The Flash Rebirth</td>
                    </tr>
                    <tr>
                        <td id="table-imp">Harley Quinn Rebirth</td>
                    </tr>
                    <tr>
                        <td id="table-imp">Batman Rebirth</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
