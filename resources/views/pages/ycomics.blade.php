@extends('layout')
@section('ycomics')
    <div class="your-heroes">
        <div class="carousel" data-flickity='{ "cellSelector": ".carousel-cell", "pageDots": false }'>
            <div class="static-banner"><h2>YOUR FAVORITE CHARACTERS</h2></div>
            <div class="carousel-cell"><h3 class="hero-title">Superman</h3><img alt="Superman" class="hero-img" src="{{ asset('img/superman.png') }}">
            </div>
            <div class="carousel-cell"><h3 class="hero-title">Batman</h3><img alt="Batman" class="hero-img" src="{{ asset('img/batman.png') }}"></div>
            <div class="carousel-cell"><h3 class="hero-title">Zoom</h3><img alt="Zoom" class="hero-img" src="{{ asset('img/zoom.png') }}"></div>
            <div class="carousel-cell"><h3 class="hero-title">Lex Luthor</h3><img alt="Lex Luthor" class="hero-img" src="{{ asset('img/lex.png') }}">
            </div>
        </div>
    </div>
    <div class="grid-container your-comics">
        <h2 class="block-title">YOUR COMICS</h2>
        <div class="grid-container comic-container">

            <div class="grid-x">
                <div style="background-color:#f2f2f2;" class="cell large-3">
                    <div class="list" >
                        <a >
                            <div id="start" class="item" onclick="openContainer(event, 'Flash')">The Flash (Rebirth)</div>
                        </a>
                        <a>
                            <div class="item" onclick="openContainer(event, 'Harley')">Harley Quinn (Rebirth)</div>
                        </a>
                        <a>
                            <div class="item" onclick="openContainer(event, 'Doomsday')">Doomsday Clock</div>
                        </a>
                    </div>
                </div>
                <div style="background-color:#f2f2f2;" class="cell large-9">
                    <div class="list">
                        <div id="Flash" class="grid-x grid-padding-x list-container tabcontent">
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #1 Comicbook Cover" src="{{ asset('img/Flash1.jpg') }}">
                                <span>The Flash #1</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #2 Comicbook Cover" src="{{ asset('img/Flash2.jpg') }}">
                                <span>The Flash #2</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #3 Comicbook Cover" src="{{ asset('img/Flash3.jpg') }}">
                                <span>The Flash #3</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #4 Comicbook Cover" src="{{ asset('img/Flash4.jpg') }}">
                                <span>The Flash #4</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #5 Comicbook Cover" src="{{ asset('img/Flash5.jpg') }}">
                                <span>The Flash #5</span>
                            </div>
                            <div class="cell small-3 large-2 c-item ">
                                <img alt="The Flash #6 Comicbook Cover" src="{{ asset('img/Flash6.jpg') }}">
                                <span>The Flash #6</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #7 Comicbook Cover" src="{{ asset('img/Flash7.jpg') }}">
                                <span>The Flash #6</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #8 Comicbook Cover" src="{{ asset('img/Flash8.jpg') }}">
                                <span>The Flash #6</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="The Flash #9 Comicbook Cover" src="{{ asset('img/Flash9.jpg') }}">
                                <span>The Flash #6</span>
                            </div>
                            <div class="cell large-12 pagination-list">
                                <div class="grid-x">
                                    <div class="cell small-4 large-4">
                                        <button type="button" class="button"><i class="fas fa-arrow-left"></i></button>
                                    </div>
                                    <div class="cell small-4 large-4">
                                        <h4>P.1</h4>
                                    </div>
                                    <div class="cell small-4 large-4">
                                        <button type="button" class="button"><i class="fas fa-arrow-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Harley" class="grid-x grid-padding-x list-container tabcontent">
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #1 Comicbook Cover" src="{{ asset('img/Harley1.jpg') }}">
                                <span>Harley Quinn #1</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #2 Comicbook Cover" src="{{ asset('img/Harley2.jpg') }}">
                                <span>Harley Quinn #2</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #3 Comicbook Cover" src="{{ asset('img/Harley3.jpg') }}">
                                <span>Harley Quinn #3</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #4 Comicbook Cover" src="{{ asset('img/Harley4.jpg') }}">
                                <span>Harley Quinn #4</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #5 Comicbook Cover" src="{{ asset('img/Harley5.jpg') }}">
                                <span>Harley Quinn #5</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #6 Comicbook Cover" src="{{ asset('img/Harley6.jpg') }}">
                                <span>Harley Quinn #6</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #7 Comicbook Cover" src="{{ asset('img/Harley7.jpg') }}">
                                <span>Harley Quinn #7</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Harley Quinn #8 Comicbook Cover" src="{{ asset('img/Harley8.jpg') }}">
                                <span>Harley Quinn #8</span>
                            </div>
                            <div class="cell large-12 pagination-list">
                                <div class="grid-x">
                                    <div class="cell small-4 large-4">
                                        <button type="button" class="button"><i class="fas fa-arrow-left"></i></button>
                                    </div>
                                    <div class="cell small-4 large-4">
                                        <h4>P.1</h4>
                                    </div>
                                    <div class="cell small-4 large-4">
                                        <button type="button" class="button"><i class="fas fa-arrow-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Doomsday" class="grid-x grid-padding-x list-container tabcontent">
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Doomsday Clock #1 Comicbook Cover" src="{{ asset('img/ddc1.jpg') }}">
                                <span>Doomsday Clock #1</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Doomsday Clock #2 Comicbook Cover" src="{{ asset('img/ddc2.jpg') }}">
                                <span>Doomsday Clock #2</span>
                            </div>
                            <div class="cell small-3 large-2 c-item">
                                <img alt="Doomsday Clock #3 Comicbook Cover" src="{{ asset('img/ddc3.jpg') }}">
                                <span>Doomsday Clock #3</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
