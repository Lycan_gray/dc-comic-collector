@extends('layout')
@section('newspost')
    <div class="header">
        <div class="grid-container" style="max-width: 1200px;">
            <div class="news-block">
                <div class="grid-container">
                    <div class="grid-x">
                        <div class="cell small-12">
                            <p class="heading-text">News</p>
                            <h3 class="heading-title">The Flash War is Coming!</h3>
                            <p class="heading-author">By <strong>Peter Boersma</strong></p>
                            <p class="heading-date"> Wednesday, June 20th, 2018</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="news-text grid-container" style="max-width: 1100px; padding: 2em;">
        <div class="grid-x" style="padding-bottom: 2em;">
            <div class="cell small-12 medium-10">
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                    tempor.</h3>
            </div>
        </div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
            nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
            anim id est laborum.</p>
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
            aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni
            dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
            sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore
            magnam aliquam quaerat voluptatem.</p>
    </div>

@endsection
