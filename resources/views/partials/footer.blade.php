<footer>
    <div class="grid-container">
        <h5>DC Comicbook Collector</h5>
        <hr>
        <div class="grid-x">
            <div class="cell small-5 large-2">
                <img src="{{asset('img/logo.png')}}">

            </div>
            <div class="cell small-7 large-5">
                <ul class="footer-menu">
                    <li class="nav-item"><a href="/">Home</a></li>
                    <li class="nav-item"><a href="news">News</a></li>
                    <li class="nav-item"><a href="your-comics">Your comics</a></li>
                </ul>
            </div>
        </div>
        <small>© Content 2018 DC Entertainment, School Assignment made by Peter Boersma</small>
    </div>
</footer>
