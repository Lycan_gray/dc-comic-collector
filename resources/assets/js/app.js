window.onload = function() {
    document.getElementById('Flash').style.display = "flex";
    document.getElementById('start').classList.add('active');
};
function openContainer(evt, containerName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("item");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(containerName).style.display = "flex";
    evt.currentTarget.className += " active";
}
